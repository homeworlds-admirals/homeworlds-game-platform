import React from 'react';
import MaterialIcon from 'material-icons-react';

const colors = {
  b: 'blue',
  g: 'green',
  y: 'yellow',
  r: 'red',
}
const pieceSizes = [
  '',
  'small',
  'medium',
  'large',
]
const pieceIcons = {
  p1: 'arrow_drop_down', //Player 1 piece
  p2: 'arrow_drop_up', // Player 2 piece
  star: 'lens', // Regular star
}

class Piece extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 0,
    }
  }

  render() {
    return (
        <MaterialIcon
          icon={pieceIcons[this.props.type]}
          size={pieceSizes[this.props.piece[1]]}
          color={colors[this.props.piece[0]]}
        />
    );
  }
}

 export default Piece;