import React from 'react';
import Piece from './Piece';

class StarSystem extends React.Component {
  /*
    Star system types:
        H1: Player 1 Homeworld
        H2: Player 2 Homeworld
        S: Star System
  */
  constructor(props) {
    super(props);
    console.log('STARSYSTEM: ', props);

    this.selectPiece = props.selectPiece;
    this.state = {
      type: this.props.type || 'S',
      pieces: this.props.pieces || {},
      starPieces: this.props.starPieces || [],
      p1Pieces: this.props.p1Pieces || [],
      p2Pieces: this.props.p2Pieces || [],
    };
    console.log(this.props.pieces);
  }

  handleClick(piece) {
    console.log('Selected ', piece);
    this.selectPiece(piece);
    // Logic to mark piece as selected
  }

  render() {
    let systemPieces =[];
    Object.entries(this.state.pieces).forEach(pieceGroup => {
      systemPieces.push(
        pieceGroup[1].map(piece => {
          let pieceClass = 'selectable';
          if (this.state.selected) {
            pieceClass += ' selected';
          }
          return (
            <span
              onClick={() => this.handleClick(piece)}
              className={pieceClass}
            >
              <Piece
                key={piece}
                piece={piece}
                type={pieceGroup[0]}
              />
            </span>
          );
        })
      );
    });
    return (
      <div
        style={{width: "100px", height: "100px", backgroundColor: "#000", border: "3px solid #fff"}}
      >
        {systemPieces}
      </div>
    );
  }
}

export default StarSystem;
