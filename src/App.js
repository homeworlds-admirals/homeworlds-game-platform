import React from 'react';
import './App.css';
import Game from './Game';

class App extends React.Component {
  render() {
    return (
      <Game />
    );
  }
}

// This allows page to update without refreshing
if (module.hot) {
  module.hot.accept();
}

export default App;
