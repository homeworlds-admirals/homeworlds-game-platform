import React from 'react';
import Bank from './Bank';
import StarSystem from './StarSystem';

class Universe extends React.Component {
  constructor(props) {
    super(props);
    console.log('UNIVERSE: ', props);
    this.state = {
      starSystems: [],
      bankPieces: [],
    }
    this.handleMove = this.handleMove.bind(this);
  }
  componentWillMount() {
    this.initUniverse();
  }

  initUniverse() {
    let initialBank = [
      'b1a','b1b','b1c','b2a','b2b','b2c','b3a','b3b','b3c',
      'g1a','g1b','g1c','g2a','g2b','g2c','g3a','g3b','g3c',
      'y1a','y1b','y1c','y2a','y2b','y2c','y3a','y3b','y3c',
      'r1a','r1b','r1c','r2a','r2b','r2c','r3a','r3b','r3c',
    ];
    let initialSystems = [
      {
        type: "H1",
        name: "p1Home",
        pieces: {
          p1: ['g3a'],
          p2: [],
          star: ['b1a', 'r3a']
        }
      },
      {
        type: "S",
        name: "emptySpace",
        pieces: {
          p1: [],
          p2: [],
          star: []
        }
      },
      {
        type: "H2",
        name: "p2Home",
        pieces: {
          p1: [],
          p2: ['g3a'],
          star: ['y1a', 'b2a']
        }
      },
    ]
    this.setState({
      bankPieces: initialBank,
      starSystems: initialSystems,
    });
  }

  handleMove() {

  }

  render() {
    return (
      <div>
        <Bank
          key="bank"
          pieces={this.state.bankPieces}
          selectPiece={this.props.selectPiece}
        />
        {this.state.starSystems.map(starSystem => {
          return (
            <StarSystem
              key={starSystem.name}
              type={starSystem.type}
              pieces={starSystem.pieces}
              starPieces={starSystem.starPieces}
              p1Pieces={starSystem.p1Pieces}
              p2Pieces={starSystem.p2Pieces}
              selectPiece={this.props.selectPiece}
            />
          );
        })}
      </div>
    );
  }
}

export default Universe;
