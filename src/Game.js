import React from 'react';
import Bank from './Bank';
import Universe from './Universe';

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPlayer: '1',
      selectedPiece: null,
      selectedStarSystemSize: null,
      history: [],
    }
    this.selectPiece = this.selectPiece.bind(this);
  }

  selectPiece(selectedPiece) {
    this.setState({selectedPiece});
    console.log('GLOBAL SELECTED: ', selectedPiece);
  }

  switchPlayer() {
    let currentPlayer = this.state.currentPlayer;
    currentPlayer = (currentPlayer === '1') ? '2' : '1';
    this.setState({currentPlayer: currentPlayer});
  }

  render() {
    return (
      <Universe
        key="Universe"
        selectPiece={this.selectPiece}
      />
    );
  }
}

export default Game;
