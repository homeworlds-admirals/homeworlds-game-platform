import React from 'react';
import MaterialIcon from 'material-icons-react';
import Piece from './Piece';

const pieceSizes = [
  '',
  'small',
  'medium',
  'large',
]

const colors = {
  b: 'blue',
  g: 'green',
  y: 'yellow',
  r: 'red',
}

const pieces = [
  'b1',
  'b2',
  'b3',
  'g1',
  'g2',
  'g3',
  'y1',
  'y2',
  'y3',
  'r1',
  'r2',
  'r3',
];

class Bank extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pieces: this.props.pieces,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.pieces !== this.props.pieces) {
      this.setState({pieces: this.props.pieces});
    }
  }

  render() {
    let pieceCounts = {
      b1: 0,
      b2: 0,
      b3: 0,
      g1: 0,
      g2: 0,
      g3: 0,
      y1: 0,
      y2: 0,
      y3: 0,
      r1: 0,
      r2: 0,
      r3: 0,
    }
    for (var i = 0; i < this.state.pieces.length; i++) {
      pieceCounts[this.state.pieces[i].substr(0,2)]++;
    }

    return (
      <div style={{backgroundColor: "#999"}}>
        <span style={{color: "White"}}>BANK:</span>
        <hr />
        {pieces.map(piece => {
          if (pieceCounts[piece] > 0) {
            return (
              <span>
                <MaterialIcon
                  icon="arrow_drop_up"
                  size={pieceSizes[piece[1]]}
                  color={colors[piece[0]]}
                />
                {'x'+pieceCounts[piece]}
              </span>
            );
          }
          
        })}
      </div>
    );
  }
}

export default Bank;
