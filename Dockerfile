FROM fedora:latest
COPY build /build
CMD ["python3", "-m", "http.server", "--directory", "/build/", "8080"]
