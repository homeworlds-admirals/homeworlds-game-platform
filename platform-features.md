Web platform for playing Homeworlds
====================================

Major features:

- support for multiple players to play multiple, simultaneous Homeworlds games together
- support "real-time" play and possibly correspondence play too
- graphical display of game states
- mouse/touch-based user interface for taking game actions
- exploration of possible "future" game states in the graphical client to allow players to visualize the results of a series of moves
- text interface for submitting game actions (for alternative clients) ??
- a "lobby" where active players can wait & join games
- chat features for players in a game or in the lobby ?
- a challenge system for non-real-time initiation of games ?
- tournaments ?
- support for real-time game spectators ?
- downloadable & searchable(?) database of game logs
- graphical browser for completed games ?
- "forking" of in-progress or completed games to allow players to play out alternate endings for games ?
- one or more Homeworlds AI players

---

Open questions:

- maximum number of players per game?
- support for game variants?
- clock system?
- what information is required during user registration?
- what information should be public vs. private?
- should RT chat messages be logged?
- should correspondence chat messages be saved or discarded at some point?
- graphics style for game display (2D pyramids, 3D pyramids, or "realistic" stars & ships ?)

------------------------------------------------------------------

Major components:

- database (NoSQL?)
- game server
- graphical browser-based client
- AI player(s)

Comments:

The last three components will all need to implement some or all of the "game logic" for Homeworlds play.  To avoid too much redundancy & possible inconsistency, it would be nice to reuse as much of the game logic as possible across components.  The difficulty is that each environment typically uses a different programming language: JavaScript for the client; PHP, Python, Perl, ASP, or Java for the server; and C/C++ for Arthur's AI.

I'm wondering if we should separate the game server into two components: one to handle requests for HTML content and another to handle requests/actions related to game state, the lobby, and chat messages.  The former can use one of the traditional web programming languages and the latter could be written in JavaScript running on something like a Node runtime (see https://nodejs.org/en/ ).  This could also have the benefit of making the real-time server much more efficient since I believe that Node applications run continuously and could maintain their state in between requests (in contrast to typical web server behavior which maintains NO state between requests).

---

DIVISION OF FUNCTIONALITY

Database functionality:

- stores the state of each active game (possibly as JSON)
- stores player profiles, stats, rankings, etc.
- stores match histories, game logs, tournament results(?)
- stores static web page content (help pages, etc.) ?
- stores correspondence game challenges
- provide search results ?

Game Server functionality:

- user registration and deletion
- user authentication (internal or external?)
- generate HTML pages for the browser-based client
- handle the creation and completion of games
- handle saving and resuming games
- respond to requests for the current game state
- process & respond to submitted game actions that change the game state
- validate game actions according to the rules
- keep the database in sync with state of active games
- maintain the "lobby" state
- relay chat messages for players in a game or in the lobby
- compile statistics and rankings
- update match histories and tournaments when games begin and end
- save game logs when games end
- determine match schedules for players in tournaments
- create/accept/delete correspondence game challenges
- send email notifications for correspondence games
- API for alternate clients ??
- admin functions (specify ...)

Browser-based Client functionality:

- user login & registration forms
- user preferences & settings forms ?
- browse static & dynamic HTML content including

    * information about Homeworlds' authors and Looney Pyramids
    * rules(?), documentation and help pages
    * player profiles, stats, rankings, etc.
    * match & tournament histories
    * list of challenges

- search (form for) static & dynamic HTML content ?
- lobby screen:

    * periodically request state changes from server
    * display a list of online players and their statuses
    * display a list of in-progress games ?
    * display recent chat messages & input chat box
    * allow players to change their status
    * allow players to invite/join games
    * allow players to start games with AI players
    * allow players to select other games to spectate ?

- game play screen:

    * periodically request state changes from server
    * display recent chat messages & input chat box
    * display a list of previous moves (in a sidebar?)
    * display a graphical representation of the game (using HTML Canvas?)
    * respond to user input for manipulating game pieces, updating the graphical display to show the results of hypothetical moves
    * display a list of hypothetical moves the player has made so far (these are not displayed to any opponents and aren't sent to the server)
    * allow player to submit a list of moves for a single turn to the server once they are satisfied
    * allow players to view previous states in the game
    * allow players to suggest forking the game from any current or previous state ?

- game browser screen:

    * similar to game play screen but used to review completed games
    * display a list of game moves (in a sidebar?)
    * display a graphical representation of the game (using HTML Canvas?)
    * allow user to view any state in the game
    * allow user to explore hypothetical alternative moves from any state
    * allow user to invite another player to play a forked game starting from any state ?
